/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoardInfo = require("./callback1.cjs");
const getList = require("./callback2.cjs");
const getCards = require("./callback3.cjs");
const cards=require("./cards.json");

function usePreviousFunc() {
  setTimeout(function () {
    getBoardInfo("mcu453ed", (info) => {
      console.log(info);
    });

    getList("mcu453ed", (info) => {
      console.log(info);
    });

    let listsArray=Object.keys(cards);
    listsArray.forEach((listId)=>getCards(listId,(info)=>{
        // console.log(`Cards of list ${listId} are ${info}`);
        console.log(info);
    }));
}, 3000);
}
module.exports=usePreviousFunc;

