/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from 
    the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const lists = require("./lists_1.json");
function returnList(boardID, callback) {
  setTimeout(function () {
    listId = Object.keys(lists);
    let getId = listId.find((element) => element === boardID);
    let listsOfGivenId = lists[getId];
    callback(listsOfGivenId);
  }, 3000);

  // console.log(listsOfGivenId);
}

module.exports = returnList;
