/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it 
    from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const cards = require("./cards.json");

function returnCards(cardId, callback) {
  setTimeout(function () {
    cardKeys = Object.keys(cards);
    let getId = cardKeys.find((element) => element === cardId);
    let cardsOfGivenListId = cards[getId];
    callback(cardsOfGivenListId);
  }, 3000);
}

module.exports = returnCards;
