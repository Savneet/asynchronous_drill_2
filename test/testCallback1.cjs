getBoardInfo = require("../callback1.cjs");

try {
  getBoardInfo("abc122dc", (info) => { //Pass the control back means callback function will print from where it is called. So control will come back to the file for printing.
    console.log(info);
  });
} catch (error) {
  console.log(error);
}
